
Calibration Instructions:

http://wiki.ros.org/openni_launch/Tutorials/IntrinsicCalibration




Commands used:

rosrun camera_calibration cameracalibrator.py image:=/camera/rgb/image_raw camera:=/camera/rgb --size 8x6 --square 0.04

rosrun camera_calibration cameracalibrator.py image:=/camera/ir/image camera:=/camera/ir --size 8x6 --square 0.04




The "depth_calibration.yaml" file was obtained by running "roslaunch depth_calibration depth_calibration.launch" (in front of a flat surface)

The file gets saved in "/home/socialrobot/.ros/camera_info", and can be loaded together with openNI2 when you run "roslaunch depth_calibration depth_adjuster.launch" (with remapped topics)