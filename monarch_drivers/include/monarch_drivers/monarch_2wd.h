#include <pthread.h>
#include <sys/time.h>
#include <iostream>
#include <termios.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cereal_port/CerealPort.h>
#include <ros/ros.h>


//Commands
#define VELOCITY_CMD		0x56
#define ENCODERS_TICKS		0x4A
#define MOTOR_CURRENT		0x50
#define MOTOR_BOARD_VOLTS	0x51
#define TEMPERATURES		0x52
#define SET_COOLING_FANS	0x53
#define GET_COOLING_FANS	0x54
#define COMM_STATUS		0x55

//For serial port
#define BAUDRATE	115200


class socialRobot{
	
public:
    socialRobot(const char* port,const char* i_port); //constructor
    virtual ~socialRobot();
    /* degrees and radians and viceverse */
    inline double DTOR(double a); //M_PI * a / 180.0
    inline double RTOD(double a); //180.0 * a / M_PI
    int setup();
    int shutdown();
    bool checksum(char data[], int length);
    int drive(double linear_speed_x, double angular_speed);
    int socialRobot_diagnostic();
    int communication_status();
    int calculateOdometry();
    void resetOdometry();
    void setOdometry(double new_x, double new_y, double new_yaw);
    double getYaw();
    void setYaw(double _yaw);
    bool getMotorDriversEnabled();
    
    double getOdometry_x();
    double getOdometry_y();
    double getOdometry_yaw();
    void setVelYaw(double _vel_th);
    double getDx();
    double getVelocity_x();
    double getVelocity_y();
    double getVelocity_th();
    void setForceStop(bool _force_stop);
    bool getForceStop();
    void getSonars();
    void getBatteries();
    void getChargingStatus();
    void getTemperatureSensor();
    void getCapacitive();
    void getBumpers();
    void undock();
    void dock();
    //LEDS
    void setIntensity(int intensity, char panel);
    void RGB_Leds_Control(char device, char Red, char Green, char Blue, char time);
    void drawMouth(int shape);
    void drawMouth(int values[8][32]);
    //int getYaw();
    double bat_motor;
    double bat_elec;
    double bat_cable; 
    double bat_pc;
    bool capacitive_sensors[5];
    bool bumpers[4];
    double sonars[12];
    double temperature;
    double humidity; 
    
private:
    //diagnostic
    bool motor_drivers_enabled;
    bool force_stop;
    //ros::NodeHandle n;
    std::string serial_port, leds_serial_port, sensor_serial_port, inertial_port;
    //int	fd;
    //odometry x
    double odometry_x_;
    //odometry y
    double odometry_y_;
    //odometry yaw
    double odometry_yaw_;
    //! Amount of distance travelled since last reading
    double distance_;
    //! Amount of angle turned since last reading
    double angle_;
    double vel_x;
    double vel_y;
    double vel_th;
	
    //! Cereal port object
    cereal::CerealPort* serial_port_;
    cereal::CerealPort* leds_serial_port_;
    cereal::CerealPort* sensor_serial_port_;
    cereal::CerealPort* inertial_serial_port_;        
    
};

//}

