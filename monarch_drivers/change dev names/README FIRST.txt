
1)
In order to have the correct "/dev" names for all the Robot's hardware and sensors, you will need to place
the "99-mbot.rules" file (which is inside the "udev rules.zip") in the "/lib/udev/rules.d/" folder.

2)
In a fresh ubuntu installation, you'll need to run: "sudo usermod -a -G dialout (username)" and reboot, 
in order to gain permission to access the serial ports. Replace (username) with your ubuntu user.



FIX UDEV RULE (IMU) IN THE SOCIAL ROBOT -- I'VE NEVER TESTED THIS:

SUBSYSTEMS=="usb",KERNEL=="ttyACM[0-9]*",ATTRS{idProduct}=="8036",ATTRS{idVendor}=="2341",ATTRS{manufacturer}=="Arduino LLC",SYMLINK+="sr-imu",GROUP="dialout"
